<?php

/**
 * @file
 * Configuration options for JAIL Formatter settings
 */

/**
 * Menu callback; Settings administration.
 */
function jail_admin_settings() {
  $form['jail_compression_type'] = array(
    '#type' => 'radios',
    '#title' => t('Choose JAIL compression level'),
    '#options' => array(
      'min' => t('Production (Minified)'),
      'none' => t('Development (Uncompressed Code)'),
    ),
    '#default_value' => variable_get('jail_compression_type', 'min'),
  );

  $form['jail_advanced_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  $form['jail_advanced_settings']['jail_placeholder_dimensions'] = array(
    '#title' => t('Add height and width attributes to placefolder image tag'),
    '#type' => 'checkbox',
    '#description' => t('When checked, the height="" and width="" attributes will be automatically populated for the placholder image specified below to match those of the final output image. It is recommended add height and with styling for img.lazy via CSS instead of enabling this option.'),
    '#default_value'  => variable_get('jail_placeholder_dimensions', 0),
  );
  $form['jail_advanced_settings']['jail_noscript'] = array(
    '#title' => t('No Script images'),
    '#type' => 'checkbox',
    '#description' => t('When checked, a &lt;noscript&gt; tag will be added containing a regular version of the file.  This will increase markup.'),
    '#default_value'  => variable_get('jail_noscript', 0),
   );
   $form['jail_advanced_settings']['jail_timeout'] = array(
     '#type'          => 'textfield',
     '#title'         => t('Timeout'),
     '#description'   => t('Number of msec after that the images will be loaded - Default: 10'),
      '#default_value' => variable_get('jail_timeout', '10'),
   );
   $form['jail_advanced_settings']['jail_effect'] = array(
     '#type'          => 'textfield',
     '#title'          => t('Effect'),
     '#description'    => t('Any jQuery effect that makes the images display - Default: NULL -- It is best to NOT use this setting for performance reasons'),
     '#default_value'  => variable_get('jail_effect', NULL),
   );
   $form['jail_advanced_settings']['jail_speed'] = array(
     '#type'          => 'textfield',
     '#title'          => t('Speed'),
     '#description'    => t('How long the animation will run - Default: 400'),
     '#default_value'  => variable_get('jail_speed', '400'),
   );
  $form['jail_advanced_settings']['jail_placeholder'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Place Holder Image'),
    '#description'   => t('Path to an image (such a loader) you want to display while waiting for the images to be loaded - Default: "transparent.png file included with the JAIL module."'),
    '#default_value' => variable_get('jail_placeholder', drupal_get_path('module', 'jail') . '/placeholder.png'),
  );
  $form['jail_advanced_settings']['jail_offset'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Offset'),
    '#description'   => t('An offset of "500" would cause any images that are less than 500px below the bottom of the window or 500px above the top of the window to load. - Default: 0'),
    '#default_value' => variable_get('jail_offset', '0'),
  );
  $form['jail_advanced_settings']['jail_hidden'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Load Hidden Imagse'),
    '#description'   => t('Boolean to load hidden images - Default: false (so hidden images are not loaded)'),
    '#default_value' => variable_get('jail_hidden', 'FALSE'),
  );
  return system_settings_form($form);
}
