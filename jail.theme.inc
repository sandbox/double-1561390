<?php

/**
 * @file
 * JAIL theme functions.
 */

/**
 * Handler for JAIL display of imagecache + imagefield CCK fields.
 *
 * @param $element
 *   The CCK field element.
 * @return
 *   HTML output for displaying the image.
 */
function theme_jail_formatter_imagefield($element) {
  if (!module_exists('imagecache') || !module_exists('imagefield')) {
    return;
  }

  if (!empty($element['#item']['fid'])) {
    $item = $element['#item'];

    if (is_string($item['data'])) {
      $item['data'] = unserialize($item['data']);
    }
    if (!isset($item['filepath'])) {
      $file = field_file_load($item['fid']);
      $item['filepath'] = $file['filepath'];
    }
    if (!empty($item['data']['alt'])) {
      $item['data']['title'] = $item['data']['alt'];
    }

    list($presetname, $modulename) = explode('__', $element['#formatter'], 2);

    if ($preset = imagecache_preset_by_name($presetname)) {

      $image = theme('imagecache', $presetname, $item['filepath'], $item['data']['alt'], $item['data']['title'], $attributes);
      $src = imagecache_create_url($presetname, $item['filepath']);
      $placeholder = check_plain(variable_get('jail_placeholder', drupal_get_path('module', 'jail') . '/placeholder.png'));
      $placeholder_dimensions = variable_get('jail_placeholder_dimensions', 0);

      if ($placeholder_dimensions == 0) {
        $output = '<img class="lazy" data-src="' . $src . '" src="' . '/' . $placeholder . '"/>';
      }
      else {
        imagecache_generate_image($presetname, $item['filepath']);
        $width = _jail_get_attribute_value('width', $image);
        $height = _jail_get_attribute_value('height', $image);
        $output = '<img class="lazy" data-src="' . $src . '" src="' . '/' . $placeholder . '" width="' . $width . '" height="' . $height . '"/>';
      }

      if (variable_get('jail_noscript', 0)) {
        $output .= '<noscript>' . $image . '</noscript>';
      }
    }
    else {
      $output = theme('imagecache', $presetname, $item['filepath'], $item['data']['alt'], $item['data']['title'], $attributes);;
    }
    return $output;
  }
}
