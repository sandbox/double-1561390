About
-----

The JAIL module adds a CCK display formatter for ImageCache presets that provides
lazy loading of images via the Javascript Image Asynchronous Loading (JAIL) jQuery plugin.

Information on the JAIL plugin: http://www.sebastianoarmelibattana.com/projects/jail


Dependencies
------------
* jquery_update
* libraries

* JqueryAsynchImageLoader (JAIL) Plugin for jQuery version 0.9.9


Recommended
-----------
* iamgechache


Installation
------------

1)  Copy the jail module folder to the modules folder in your installation.
    For example, sites/all/modules.

2)  Enable the module in Drupal under:
    Administer -> Site building -> Modules (/admin/build/modules).

3)  Download and extract the JAIL plugin files into "sites/all/libraries/jail".
    Link: https://github.com/sebarmeli/JAIL

4)  Add one or more ImageCache presets to your site:
    Administer -> Site building -> ImageCache (admin/build/imagecache).

5)  The JAIL display formatters should now be available for ImageField images under:
    Administer -> Content management -> Content types -> [type] -> Display Fields
    (admin/content/node-type/[type]/display).

    The JAIL display formatters are also available in Views for ImageField images.


Configuration
-------------

Configuration options are available under:
Administer -> Site configuration -> JqueryAsynchImageLoader (JAIL) (/admin/settings/jail).